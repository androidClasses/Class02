package edu.viniciussdsilva.aula2app;

/**
 * Created by viniciussdsilva on 06/03/2018.
 */

public enum TemperatureType {
    CELSIUS, FAHRENHEIT;

    public String toString() {
        switch (this) {
            case CELSIUS:
                return "Celsius";
            case FAHRENHEIT:
                return "Fahrenheit";
            default:
                return "";
        }
    }

    public String degreeSymbol() {
        return (this == TemperatureType.CELSIUS)
                ? "˚C"
                : "˚F";
    }

    public TemperatureType otherType() {
        return (this == CELSIUS)
                ? FAHRENHEIT
                : CELSIUS;
    }

}

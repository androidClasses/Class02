package edu.viniciussdsilva.aula2app;

import java.io.Serializable;

/**
 * Created by viniciussdsilva on 07/03/2018.
 */

public class Temperature implements Serializable {

    private Double value;
    private TemperatureType type;

    public Temperature(Double value, TemperatureType type) {
        this.value = value;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Temperature{" +
                "value=" + value +
                ", type=" + type +
                ", convertedValue=" + convertedTemperature() +
                '}';
    }

    public Double convertedTemperature() {
        return (this.type == TemperatureType.CELSIUS)
                ? (double) Math.round((((value * 9) / 5) + 32) * 100) / 100
                : (double) Math.round((((value - 32) * 5) / 9) * 100) / 100;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public TemperatureType getType() {
        return type;
    }

    public void setType(TemperatureType type) {
        this.type = type;
    }
}

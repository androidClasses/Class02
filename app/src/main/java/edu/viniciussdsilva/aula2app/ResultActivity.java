package edu.viniciussdsilva.aula2app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    // Variables
    private Temperature temperature;
    private String standardToConvertedTxt;

    // Outlets
    private TextView originalTemperatureTxt;
    private TextView toConvertedTxt;
    private TextView convertedTemperatureTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        connectingToActivity();
        instancingParameters();

        updateUI();
        Log.d("TEMPERATURE", temperature.toString());
        Log.d("EXERCISE1", "Inside method onCreate on ResultActivity");
    }

    private void updateUI() {
        originalTemperatureTxt.setText(String.valueOf(temperature.getValue()) + temperature.getType().degreeSymbol());
        toConvertedTxt.setText(standardToConvertedTxt.replace("#TO_CONVERTED_TEMPERATURE#", temperature.getType().otherType().toString()));
        convertedTemperatureTxt.setText(String.valueOf(temperature.convertedTemperature() + temperature.getType().otherType().degreeSymbol()));
    }

    private void instancingParameters() {
        standardToConvertedTxt = "in #TO_CONVERTED_TEMPERATURE# is";
        Intent intent = getIntent();
        temperature = (Temperature) intent.getSerializableExtra("temperature");
        Log.d("TEMPERATURE", temperature.toString());
    }

    private void connectingToActivity() {
        originalTemperatureTxt = findViewById(R.id.originalTemperatureTxt);
        toConvertedTxt = findViewById(R.id.toConvertedTxt);
        convertedTemperatureTxt = findViewById(R.id.convertedTemperatureTxt);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("EXERCISE1", "Inside method onStart on ResultActivity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("EXERCISE1", "Inside method onResume on ResultActivity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("EXERCISE1", "Inside method onPause on ResultActivity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("EXERCISE1", "Inside method onStop on ResultActivity");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("EXERCISE1", "Inside method onDestroy on ResultActivity");
    }
}

package edu.viniciussdsilva.aula2app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    // Variables
    private String standardTemperatureToConverterTxt;
    private String standardHintTemperatureValueET;

    // Outlets
    private EditText temperatureValueET;
    private TextView temperatureToConvertTxt;
    private ToggleButton temperatureTypeToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectingToActivity();

        standardTemperatureToConverterTxt = "#TEMPERATURE1# to #TEMPERATURE2# converter!";
        standardHintTemperatureValueET = "Type a #TEMPERATURE# temperature";

        Log.d("EXERCISE1", "Inside method onCreate on MainActivity");
    }

    public void toResultActivity(View view) {
        TemperatureType temperatureType = (temperatureTypeToggle.isChecked())
                ? TemperatureType.CELSIUS
                : TemperatureType.FAHRENHEIT;
        Temperature temperature = new Temperature(Double.valueOf(temperatureValueET.getText().toString()), temperatureType);
        if (temperature.getValue() == null) {
            temperature.setValue(0.0);
        }
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("temperature", temperature);
        startActivity(intent);
    }

    private void connectingToActivity() {
        temperatureValueET = findViewById(R.id.temperatureValueET);
        temperatureToConvertTxt = findViewById(R.id.temperatureToConvertTxt);
        temperatureTypeToggle = findViewById(R.id.temperatureTypeToggle);
        temperatureTypeToggle.setTextOn(TemperatureType.CELSIUS.toString());
        temperatureTypeToggle.setTextOff(TemperatureType.FAHRENHEIT.toString());
        temperatureTypeToggle.setChecked(true);
    }

    public void temperatureTypeTogglePressed(View view) {
        updateTexts((temperatureTypeToggle.isChecked()) ? TemperatureType.CELSIUS : TemperatureType.FAHRENHEIT);
    }

    private void updateTexts(TemperatureType type) {
        temperatureToConvertTxt.setText(temperatureToConvertTxt(type, type.otherType()));
        temperatureValueET.setHint(hintTemperatureValueET(type));
    }

    private String temperatureToConvertTxt(TemperatureType from, TemperatureType to) {
        return standardTemperatureToConverterTxt
                .replace("#TEMPERATURE1#", from.toString())
                .replace("#TEMPERATURE2#", to.toString());
    }

    private String hintTemperatureValueET(TemperatureType type) {
        return standardHintTemperatureValueET.replace("#TEMPERATURE#", type.toString());
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("EXERCISE1", "Inside method onStart on MainActivity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("EXERCISE1", "Inside method onResume on MainActivity");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("EXERCISE1", "Inside method onPause on MainActivity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("EXERCISE1", "Inside method onStop on MainActivity");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("EXERCISE1", "Inside method onDestroy on MainActivity");
    }
}
